import { Injectable, Inject } from '@nestjs/common';
import { fork, ChildProcess } from 'child_process';
import { WorkerMessage, WorkerStartMessage, WorkerMessageType, WorkerErrorMessage } from './worker-message';
import { WorkerState } from './worker-state';
import { ITaskQueue } from 'src/tasks/task-queue';
import { ITaskData } from '../tasks/task-data';
import { WorkersManagerConfiguration } from './workers-manager.configuration';

@Injectable()
export class WorkersManager {
  private started: boolean = false;
  private workersPool: WorkerPoolRecord[] = [];

  public constructor(
    @Inject(WorkersManagerConfiguration) private readonly config: WorkersManagerConfiguration,
    @Inject('ITaskQueue') private readonly taskQueue: ITaskQueue,
  ) {

  }

  public start(): void {
    if (this.started) {
      return;
    }
    this.started = true;

    for (let i = 0; i < this.config.childProcessesCount; i++) {
      this.workersPool.push(this.createWorker());
    }

    this.taskQueue.onAdd(this.notify);
  }

  public getWorkersStates() {
    return this.workersPool.map((e) => ({ state: e.state, taskData: e.taskData, pid: e.worker.pid }));
  }

  private notify = (): void => {
    let idleWorker: WorkerPoolRecord;
    // tslint:disable-next-line: no-conditional-assignment
    while (idleWorker = this.workersPool.find((w) => w.state === WorkerState.Idle)) {
      const taskData = this.taskQueue.poll();
      if (!taskData) {
        return;
      }
      this.startTask(idleWorker, taskData);
    }
  }

  private createWorker(): WorkerPoolRecord {
    const worker = fork(this.config.workerFile);

    const record = {
      worker,
      state: WorkerState.Idle,
      taskData: null,
      startTime: null,
    };

    worker.on('error', (error: Error) => {
      // tslint:disable-next-line: no-console
      console.log(`Worker ${record.worker.pid}: Error message from worker`, error);
    });

    worker.on('message', (msg: WorkerMessage) => {
      // tslint:disable-next-line: no-console
      console.log(`Worker ${record.worker.pid}: Message from worker`, msg);

      if (msg.message === WorkerMessageType.TaskStarted) {
        // tslint:disable-next-line: no-console
        console.log(`Worker ${record.worker.pid}: Worker has started task`, record.taskData);
        record.startTime = new Date();
      }

      if (msg.message === WorkerMessageType.Finished) {
        const spentSeconds = (new Date().getTime() - record.startTime.getTime()) / 1000;
        // tslint:disable-next-line: no-console
        console.log(`Worker ${record.worker.pid}: Worker has finished task. Spent ${spentSeconds}s`, record.taskData);
        this.taskFinished(record);
      }

      if (msg.message === WorkerMessageType.Error) {
        // tslint:disable-next-line: no-console
        console.log(`Worker ${record.worker.pid}: Error message from worker`, record.taskData, (msg as WorkerErrorMessage).error);
        this.taskFinished(record);
      }
    });

    return record;
  }

  private taskFinished(record: WorkerPoolRecord) {
    record.state = WorkerState.Idle;
    record.taskData = null;
    this.notify();
  }

  private startTask(record: WorkerPoolRecord, taskData: ITaskData) {
    record.state = WorkerState.Active;
    const msg: WorkerStartMessage = {
      message: WorkerMessageType.Start,
      taskData,
    };
    record.taskData = taskData;
    record.worker.send(msg);
  }
}

interface WorkerPoolRecord {
  worker: ChildProcess;
  state: WorkerState;
  taskData: ITaskData;
  startTime: Date;
}

import { Injectable } from '@nestjs/common';

export class MongoConfig {
  public url: string;
  public db: string;
}

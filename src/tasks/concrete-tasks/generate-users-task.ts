import { Task } from '../task';
import { v1 } from 'node-uuid';
import { UserModel } from 'src/dal/user.model';
import { IUsersRepository } from '../../dal/users.repository';

export class GenerateUsersTask extends Task {
  public constructor(
    private readonly repository: IUsersRepository,
  ) {
    super();
  }

  run(taskParams: { quantity: number }): void {
    this.do(taskParams.quantity);
  }

  private async do(quantity: number) {
    for (let i = 0; i < quantity; i++) {
      const user = new UserModel();
      user.uuid = v1();
      user.email = v1() + '@mail.com';
      user.dateOfBirth = new Date();
      user.lastUpdate = new Date();
      user.gender = Math.random() > 0.5 ? 'male' : 'female';
      await this.repository.insert(user);
    }

    this.sendFinish();
  }
}

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { WorkersManager } from './workers/workers.manager';
import * as dotenv from 'dotenv';
import { WorkersManagerConfiguration } from './workers/workers-manager.configuration';

async function bootstrap() {
  dotenv.config();

  const app = await NestFactory.create(AppModule);

  const workersManager = app.get(WorkersManager);

  const config = app.get(WorkersManagerConfiguration);
  config.childProcessesCount = Number(process.env.CHILD_PROCESSES_COUNT);
  config.workerFile = process.env.WORKER_FILE;

  workersManager.start();

  await app.listen(3000);
}
bootstrap();

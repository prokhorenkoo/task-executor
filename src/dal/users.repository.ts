import { UserModel } from './user.model';

export interface IUsersRepository {
  getAll(skip: number, take: number, lastUpdateFrom: Date): Promise<UserModel[]>;
  insert(user: UserModel): Promise<void>;
  update(user: UserModel): Promise<void>;
  get(uuid: string): Promise<UserModel>;
}

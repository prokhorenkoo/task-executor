import { Injectable } from '@nestjs/common';

@Injectable()
export class WorkersManagerConfiguration {
  public childProcessesCount: number;
  public workerFile: string;
}

import { TaskType } from './task-type';

export interface ITaskData {
  taskType: TaskType;
  taskParams: { [key: string]: any };
}

CREATE DATABASE IF NOT EXISTS shop;
CREATE TABLE IF NOT EXISTS shop.users (
	uuid varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	lastUpdate DATETIME NOT NULL,
	dateOfBirth DATETIME NOT NULL,
	gender varchar(100) NULL,
	CONSTRAINT users_PK PRIMARY KEY (uuid)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_0900_ai_ci;

use shop;
ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY 'my-secret-pw';
flush privileges;
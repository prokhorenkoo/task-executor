import { MongoClient, Collection, Cursor } from 'mongodb';
import { IUsersRepository } from './users.repository';
import { UserModel } from './user.model';
import { MongoConfig } from './mongo.config';

export class UsersMongoRepository implements IUsersRepository {

  public constructor(
    private readonly config: MongoConfig,
  ) { }

  public async get(uuid: string): Promise<UserModel> {
    const mc = await this.getMongoClient();
    const collection = await this.getCollection(mc);
    const array = await collection
      .find({ uuid })
      .limit(1)
      .toArray();
    const result = array[0];
    await mc.close();
    return result;
  }

  public async getAll(skip: number, take: number, lastUpdateFrom: Date): Promise<UserModel[]> {
    const mc = await this.getMongoClient();
    const collection = await this.getCollection(mc);
    let query: Cursor<UserModel>;
    if (lastUpdateFrom) {
      query = collection.find({ lastUpdate: { $gte: lastUpdateFrom } });
    } else {
      query = collection.find();
    }
    const result = await query
      .sort({ uuid: 1 })
      .skip(skip)
      .limit(take)
      .toArray();
    await mc.close();
    return result;
  }

  public async insert(user: UserModel): Promise<void> {
    const mc = await this.getMongoClient();
    const collection = await this.getCollection(mc);
    await collection.insertOne(user);
    await mc.close();
  }

  public async update(user: UserModel): Promise<void> {
    const mc = await this.getMongoClient();
    const collection = await this.getCollection(mc);
    await collection.updateOne({ uuid: user.uuid }, { $set: user });
    await mc.close();
  }

  private getMongoClient(): Promise<MongoClient> {
    return MongoClient.connect(this.config.url, { useUnifiedTopology: true });
  }

  private getCollection(mc: MongoClient): Collection<UserModel> {
    const db = mc.db(this.config.db);
    return db.collection('users');
  }
}

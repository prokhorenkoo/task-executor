import { TaskType } from 'src/tasks/task-type';
import { ITaskData } from 'src/tasks/task-data';

export interface WorkerMessage {
  message: WorkerMessageType;
}

export interface WorkerStartMessage extends WorkerMessage {
  message: WorkerMessageType.Start;
  taskData: ITaskData;
}

export interface WorkerErrorMessage extends WorkerMessage {
  message: WorkerMessageType.Error;
  error: string;
}

export interface WorkerIsActiveErrorMessage extends WorkerMessage {
  message: WorkerMessageType.WorkerIsActiveError;
}

export interface WorkerFinishedMessage extends WorkerMessage {
  message: WorkerMessageType.Finished;
}

export interface WorkerTaskStartedMessage extends WorkerMessage {
  message: WorkerMessageType.TaskStarted;
}

export enum WorkerMessageType {
  Start = 'Start',
  Error = 'Error',
  WorkerIsActiveError = 'WorkerIsActiveError',
  Finished = 'Finished',
  TaskStarted = 'TaskStarted',
}

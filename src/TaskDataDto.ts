import { ITaskData } from './tasks/task-data';
import { TaskType } from './tasks/task-type';

export class TaskDataDto implements ITaskData {
  taskType: TaskType;
  taskParams: { [key: string]: any; };
}

import { createConnection, QueryOptions } from 'mysql';
import { IUsersRepository } from './users.repository';
import { UserModel } from './user.model';
import { MySqlConfig } from './mysql.config';

export class UsersMySqlRepository implements IUsersRepository {

  public constructor(
    private readonly config: MySqlConfig,
  ) { }

  public async get(uuid: string): Promise<UserModel> {
    const result = await this.makeQuery({
      sql: 'SELECT uuid, email, lastUpdate, dateOfBirth, gender FROM shop.users WHERE uuid = ? LIMIT 1;',
      values: [uuid],
    });
    return result[0];
  }

  public async getAll(skip: number, take: number, lastUpdateFrom: Date): Promise<UserModel[]> {
    const result = await this.makeQuery({
      sql: `SELECT uuid, email, lastUpdate, dateOfBirth, gender FROM shop.users WHERE ? IS NULL OR lastUpdate >= ? LIMIT ?, ?;`,
      values: [lastUpdateFrom, lastUpdateFrom, skip, take],
    });
    return result as UserModel[];
  }

  public async insert(user: UserModel): Promise<void> {
    await this.makeQuery({
      sql: 'INSERT INTO shop.users (uuid, email, lastUpdate, dateOfBirth, gender) VALUES(?, ?, ?, ?, ?);',
      values: [user.uuid, user.email, user.lastUpdate, user.dateOfBirth, user.gender],
    });
  }

  public async update(user: UserModel): Promise<void> {
    await this.makeQuery({
      sql: 'UPDATE shop.users SET email = ?, lastUpdate = ?, dateOfBirth = ?, gender = ? WHERE uuid = ?;',
      values: [user.email, user.lastUpdate, user.dateOfBirth, user.gender, user.uuid],
    });
  }

  private makeQuery(options: string | QueryOptions) {
    const connection = createConnection(this.config.url);
    return new Promise((resolve, reject) => {
      connection.query(options, (error, results, fields) => {
        connection.destroy();
        if (error) {
          // tslint:disable-next-line: no-console
          console.error(error);
          reject(error);
        } else {
          resolve(results);
        }
      });
    });
  }

}

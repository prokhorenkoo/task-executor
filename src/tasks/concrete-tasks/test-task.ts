import { Task } from '../task';

export class TestTask extends Task {
  run(taskParams: { [key: string]: any }): void {
    this.do(taskParams.id);
  }

  private async do(id: number) {
    const newPromise = () => new Promise((resolve) => {
      process.send(`From pid=${process.pid}!, taskId=${id}`);
      setTimeout(resolve, 1000);
    });

    for (let i = 0; i < id; i++) {
      await newPromise();
    }

    this.sendFinish();
  }
}

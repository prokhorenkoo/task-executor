export enum TaskType {
    Test = 'Test',
    GenerateMongoUsers = 'GenerateMongoUsers',
    GenerateMySqlUsers = 'GenerateMySqlUsers',
    SyncMongoUsers = 'SyncMongoUsers',
    SyncMySqlUsers = 'SyncMySqlUsers',
}

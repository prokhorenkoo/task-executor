import { Controller, Get, Post, Injectable, Inject, Body, BadRequestException } from '@nestjs/common';
import { AppService } from './app.service';
import { TaskDataDto } from './TaskDataDto';

@Controller()
export class AppController {
  constructor(@Inject(AppService) private readonly appService: AppService) { }

  @Get('ping')
  public getPing(): string {
    return new Date().toString();
  }

  @Post('tasks/create')
  public createTask(@Body() taskData: TaskDataDto): void {
    try {
      this.appService.createTask(taskData);
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  @Get('stats')
  public getStats() {
    return this.appService.getStats();
  }
}

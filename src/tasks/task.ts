export abstract class Task {
  errorHandler: (error: Error) => void;
  finishHandler: () => void;

  abstract run(taskParams: { [key: string]: any }): void;

  public onError(errorHandler: (error: Error) => void): void {
    this.errorHandler = errorHandler;
  }

  public onFinish(finishHandler: () => void): void {
    this.finishHandler = finishHandler;
  }

  protected sendError(error: Error): void {
    if (this.errorHandler) {
      this.errorHandler(error);
    }
  }

  protected sendFinish(): void {
    if (this.finishHandler) {
      this.finishHandler();
    }
  }
}

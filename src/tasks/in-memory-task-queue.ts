import { ITaskQueue } from './task-queue';
import { ITaskData } from './task-data';
import { Injectable } from '@nestjs/common';

@Injectable()
export class InMemoryQueue implements ITaskQueue {
  private addHandler: () => void;
  private queueArray = new Array<ITaskData>();

  public add(taskData: ITaskData) {
    this.queueArray.push(taskData);
    this.addHandler();
  }

  public onAdd(handler: () => void): void {
    this.addHandler = handler;
  }

  public poll(): ITaskData {
    return this.queueArray.shift();
  }

  getAll(): ITaskData[] {
    return [...this.queueArray];
  }
}

export class UserModel {
  public uuid: string;
  public email: string;
  public lastUpdate: Date;
  public dateOfBirth: Date;
  public gender: string;
}

import * as WorkerMessage from './worker-message';
import { TaskFactory } from 'src/tasks/task.factory';
import { TaskType } from 'src/tasks/task-type';
import { WorkerState } from './worker-state';
import { WorkerTaskStartedMessage } from './worker-message';

class TaskWorker {
  private state: WorkerState = WorkerState.Idle;

  public constructor(
    private taskFactory: TaskFactory,
  ) {

  }

  private onMessage = (message: WorkerMessage.WorkerMessage): void => {
    if (message.message === WorkerMessage.WorkerMessageType.Start) {
      this.startTask(message as WorkerMessage.WorkerStartMessage);
    }
  }

  private startTask(message: WorkerMessage.WorkerStartMessage) {
    if (this.state !== WorkerState.Idle) {
      const errorMessage: WorkerMessage.WorkerIsActiveErrorMessage = {
        message: WorkerMessage.WorkerMessageType.WorkerIsActiveError,
      };
      this.sendMessage(errorMessage);
    }
    try {
      const task = this.taskFactory.create(message.taskData.taskType);

      task.onError(this.onError);
      task.onFinish(this.onFinish);

      task.run(message.taskData.taskParams);

      const startedMessage: WorkerTaskStartedMessage = {
        message: WorkerMessage.WorkerMessageType.TaskStarted,
      };
      this.sendMessage(startedMessage);
    } catch (e) {
      this.onError(e);
    }
  }

  private sendMessage(message: WorkerMessage.WorkerMessage) {
    process.send(message);
  }

  private onError = (error: Error) => {
    const errorMessage: WorkerMessage.WorkerErrorMessage = {
      message: WorkerMessage.WorkerMessageType.Error,
      error: error.message,
    };
    this.sendMessage(errorMessage);
    this.state = WorkerState.Idle;
  }

  private onFinish = () => {
    const errorMessage: WorkerMessage.WorkerFinishedMessage = {
      message: WorkerMessage.WorkerMessageType.Finished,
    };
    this.sendMessage(errorMessage);
    this.state = WorkerState.Idle;
  }

  public start(): void {
    process.on('message', this.onMessage);
  }
}

const factory = new TaskFactory();
const w = new TaskWorker(factory);
w.start();

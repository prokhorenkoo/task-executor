import { Injectable, Inject } from '@nestjs/common';
import { ITaskQueue } from './tasks/task-queue';
import { WorkersManager } from './workers/workers.manager';
import { ITaskData } from './tasks/task-data';
import { TaskType } from './tasks/task-type';

@Injectable()
export class AppService {
  public constructor(
    @Inject('ITaskQueue') private queue: ITaskQueue,
    @Inject(WorkersManager) private workersManager: WorkersManager,
  ) { }

  public createTask(taskData: ITaskData): void {
    if (!taskData) {
      throw new Error('taskData is empty');
    }
    if (!Object.values(TaskType).includes(taskData.taskType)) {
      throw new Error(`Unknown TaskType ${taskData.taskType}`);
    }
    this.queue.add(taskData);
  }

  public getStats() {
    return {
      workers: this.workersManager.getWorkersStates(),
      queue: this.queue.getAll(),
    };
  }
}

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { InMemoryQueue } from './tasks/in-memory-task-queue';
import { WorkersManagerConfiguration } from './workers/workers-manager.configuration';
import { WorkersManager } from './workers/workers.manager';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [
    AppService,
    { provide: 'ITaskQueue', useClass: InMemoryQueue },
    WorkersManagerConfiguration,
    WorkersManager,
    // InMemoryQueue,
  ],
})
export class AppModule { }

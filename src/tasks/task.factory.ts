import { TaskType } from './task-type';
import { Task } from './task';
import { TestTask } from './concrete-tasks/test-task';
import { GenerateUsersTask } from './concrete-tasks/generate-users-task';
import { MongoConfig } from 'src/dal/mongo.config';
import { UsersMongoRepository } from 'src/dal/users.mongo.repository';
import { MySqlConfig } from 'src/dal/mysql.config';
import { UsersMySqlRepository } from 'src/dal/users.mysql.repository';
import { SyncUsersTask } from './concrete-tasks/sync-users-task';

export class TaskFactory {
  public create(taskType: TaskType): Task {
    switch (taskType) {
      case TaskType.Test:
        return this.createTestTask();
      case TaskType.GenerateMongoUsers:
        return this.createGenerateMongoUsersTask();
      case TaskType.GenerateMySqlUsers:
        return this.createGenerateMySqlUsersTask();
      case TaskType.SyncMongoUsers:
        return this.createSyncMongoUsersTask();
      case TaskType.SyncMySqlUsers:
        return this.createSyncMySqlUsersTask();
      default:
        throw new Error(`TaskFactory.create() incoming TaskType is unknown.`);
    }
  }

  private createTestTask() {
    return new TestTask();
  }

  private createGenerateMongoUsersTask() {
    return new GenerateUsersTask(this.getMongoRepository());
  }

  private createGenerateMySqlUsersTask() {
    return new GenerateUsersTask(this.getMySqlRepository());
  }

  private createSyncMongoUsersTask() {
    return new SyncUsersTask(this.getMySqlRepository(), this.getMongoRepository());
  }

  private createSyncMySqlUsersTask() {
    return new SyncUsersTask(this.getMongoRepository(), this.getMySqlRepository());
  }

  private getMongoRepository() {
    const config = new MongoConfig();
    config.url = process.env.MONGO_URL;
    config.db = process.env.MONGO_DB;
    return new UsersMongoRepository(config);
  }

  private getMySqlRepository() {
    const config = new MySqlConfig();
    config.url = process.env.MYSQL_URL;
    return new UsersMySqlRepository(config);
  }
}

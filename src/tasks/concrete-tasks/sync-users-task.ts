import { Task } from '../task';
import { UserModel } from 'src/dal/user.model';
import { IUsersRepository } from '../../dal/users.repository';

export class SyncUsersTask extends Task {
  public constructor(
    private readonly source: IUsersRepository,
    private readonly destination: IUsersRepository,
  ) {
    super();
  }

  run(taskParams: { batch: number, lastUpdateFrom: Date, forceUpdate: boolean }): void {
    this.do(taskParams.batch, new Date(taskParams.lastUpdateFrom), taskParams.forceUpdate);
  }

  private async do(batch: number, lastUpdateFrom: Date, forceUpdate: boolean) {
    let skip = 0;
    let users: UserModel[];
    // tslint:disable-next-line: no-conditional-assignment
    while ((users = await this.source.getAll(skip, batch, lastUpdateFrom)).length) {
      skip += batch;
      await Promise.all(users.map(u => this.updateUser(u, forceUpdate)));
    }

    this.sendFinish();
  }

  private async updateUser(user: UserModel, forceUpdate: boolean): Promise<void> {
    const destinationUser = await this.destination.get(user.uuid);
    if (!destinationUser) {
      await this.destination.insert(user);
    } else if (forceUpdate || destinationUser.lastUpdate < user.lastUpdate) {
      await this.destination.update(user);
    }
  }
}

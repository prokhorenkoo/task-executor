import { ITaskData } from './task-data';

export interface ITaskQueue {
  add(taskData: ITaskData): void;
  onAdd(handler: () => void): void;
  poll(): ITaskData;
  getAll(): ITaskData[];
}
